package preconditions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@SuppressWarnings("FieldCanBeLocal")
public class CategoriesPage {
    private WebDriver driver;

    CategoriesPage(WebDriver driver) {
        this.driver = driver;
    }

    //Locators
    private String addCategoryLocator = "page-header-desc-category-new_category";

    public void clickAddCategory() {
        driver.findElement(By.id(addCategoryLocator)).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleContains("категории > Добавить"));
        new AddCategoryPage(driver);
    }
}
